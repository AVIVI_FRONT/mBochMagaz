function dataDropdown(){
    var btn = '[data-dropdown-btn]';
    if(btn.length){
        $(document).on('click', btn, clickHandler);
    }else{
        $(document).off('click', btn, clickHandler);
    }
    function clickHandler(){
        var id = $(this).attr('data-dropdown-btn');
        if(!$(this).hasClass('active')){
            openDD(id);
        }else{
            closeDD(id);
        }
    }
    var close = '[data-dropdown-close]';
    if(close.length){
        $(document).on('click', close, closeHandler);
    }else{
        $(document).off('click', close, closeHandler);
    }
    function closeHandler(){
        var id = $(this).attr('data-dropdown-close');
        if(!$(this).hasClass('active')){
            closeDD(id);
        }
    }
}
function openDD(id) {
    $('[data-dropdown-box]').removeClass('active');
    $('[data-dropdown-btn]').removeClass('active');
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.addClass('active');
    box.addClass('active');
    $('.overlay').fadeIn();
}
function closeDD(id) {
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.removeClass('active');
    box.removeClass('active');
    $('.overlay').fadeOut();
}
function removeBasketItem() {
    $(document).on('click', '.js-staff-del', function () {
        var button = $(this);
        button.parents('.staff').remove();
    })
}

function openGuide() {
    $('.card__guide').on('click', function (e) {
        e.preventDefault();
        $('.guide-size').addClass('active');
        $('.overlay').fadeIn();
    });
    $('.guide-size .menus__close').on('click', function () {
        $('.guide-size').removeClass('active');
        $('.overlay').fadeOut();
    })
}

function openBasketTab() {
    $(document).on('click', '.js-tab-closer', function () {
        var button = $(this);
        var cross = button.find('.tabb__closer');
        var block = button.parents('.tabb').find('.js-tab-inputs');
        if (button.hasClass('active')) {
        } else {
            $('.js-tab-inputs').removeClass('active');
            $('.js-tab-closer').removeClass('active');
            $('.tabb__closer').removeClass('active');
            button.addClass('active');
            block.addClass('active');
            cross.addClass('active');
        }
    });
}
function topSliderInit() {
   var el = $('.js-top__slider');
   if(el.length > 0){
       el.slick({
           autoplay: true,
           autoplaySpeed: 2000,
           slidesToShow: 1,
           arrows: false,
       });
   }
}
function slidersInit() {
    var navigator = $('.js-nav-slider');
    var shower = $('.js-show-slider');
    if (navigator.length > 0) {
        navigator.slick({
            vertical: true,
            slidesToShow: 5,
            arrows: false,
            centerMode: true,
            asNavFor: '.js-show-slider',
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })
    }
    if (shower.length > 0) {
        shower.slick({
            slidesToShow: 1,
            asNavFor: '.js-nav-slider',
            prevArrow: '<div class="card__arrow"></div>',
            nextArrow: '<div class="card__arrow card__arrow--next"></div>'
        })
    }
}
function scrollDownDev() {
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 1) {

        }
    });
}

function menusCollapse() {
    $('.menus__nav li.has-child > a').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.has-child').toggleClass('opened');
    })
}

function openFooterNav() {
    $('.footer__nav-btn').on('click', function () {
        $(this).toggleClass('active');
        $('.footer__nav').toggleClass('active');
    })
}

function contactTextOpen() {
    $('.contact__text').on('click', function () {
        $(this).toggleClass('active');
    });
    $('.contact__text:after').on('click',function () {
        $('.contact__text').removeClass('active');
    });
}

function overlayClick() {
    $('.overlay').on('click', function () {
        console.log('click');
        $('.overlay').fadeOut();
        $('*').removeClass('active');
    })
}

function personalTab() {
    $('[data-tab]').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('[data-tab]').removeClass('current');
        $('.personal__content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })
}

function viewedSliderInit() {
    $('.viewed__slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
}

function favoritesDel() {
    $('.personal__favorites-delete').on('click',function () {
        var btn = $(this)
        btn.parents('.catalog__item').remove();
    })
}

function placesTabs() {
    $('ul.location__tabs').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('current').siblings().removeClass('current')
            .closest('.location').find('.location__info').removeClass('current').eq($(this).index()).addClass('current');
    })
}

$(document).ready(function () {
    dataDropdown();
    topSliderInit();
    openBasketTab();
    removeBasketItem();
    slidersInit();
    scrollDownDev();
    openGuide();
    menusCollapse();
    openFooterNav();
    contactTextOpen();
    overlayClick();
    $('.styled select').styler({selectSmartPositioning:false});
    placesTabs();

    personalTab();
    viewedSliderInit();
    favoritesDel();

});